import { login } from "../login.js";

test("envoie du message d'erreur si il n'y a pas d'identifiant", function(){
    expect(login('','password')).toEqual({
        message: 'Missing username',
        loggedIn: false
    })
})


test("envoie du message d'erreur si il n'y a pas de mot de passe", function(){
    const result = login('admin', '')
    expect(result.message).toBe('Missing password')
    expect(result.loggedIn).not.toBe(true)
})