export function login(username, password) {
    if(!username) {
        return {
            message: 'Missing username',
            loggedIn: false
        };
    }
    if(!password) {
        return {
            message: 'Missing password',
            loggedIn: false
        };
    }
    if (username === 'admin' && password === 'admin') {
        return {
            message: 'Logged in',
            loggedIn: true
        };
    }
    return {
        message: 'Wrong username or password',
        loggedIn: false
    }
}
